import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './users/login/login.component';
import { HomeComponent } from './home/home.component';
import { AuthGuard } from './_helpers/auth.guard';
import { LogoutComponent } from './users/logout/logout.component';


const appRoutes: Routes = [
  {path: "", component: HomeComponent, canActivate: [AuthGuard]},
  {path: "logout", component: LogoutComponent, canActivate: [AuthGuard]},
  {path: "login", component: LoginComponent}
  
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
