import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { BehaviorSubject, Observable } from 'rxjs';
import { Router } from '@angular/router';
import { JwtToeken } from '../models/JwtToken';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private readonly API_LOGIN_URL = '/api/auth/authenticate';
  private readonly API_REFRESH_URL = '/api/auth/refresh-token';
  private readonly API_LOGOUT_URL = '/api/auth/revoke-token';

  public readonly REFRESH_TOKEN_KEY = "refreshToken";

  private userSubject: BehaviorSubject<JwtToeken>;
  private user: Observable<JwtToeken>;


  constructor(private http: HttpClient,
    private router: Router) {
    this.userSubject = new BehaviorSubject<JwtToeken>(null);
    this.user = this.userSubject.asObservable();
  }

  public get userValue(): JwtToeken {
    return this.userSubject.value;
  }

  public login(user: string, password: string) {

    return this.http.post<any>(this.API_LOGIN_URL, { userName: user, password: password }, { withCredentials: true })
      .pipe(map((data) => {
        localStorage.setItem(this.REFRESH_TOKEN_KEY, data.refreshToken);
        this.userSubject.next(data);
        this.startRefreshTokenTimer();
        return data;
      }));
  }

  public refreshToken() {
    var refreshToken = localStorage.getItem(this.REFRESH_TOKEN_KEY);
    if (refreshToken == null || refreshToken.length == 0) {
      return new Observable((obs) => obs.error());
    }

    return this.http.post<any>(this.API_REFRESH_URL, { "refreshToken": refreshToken }, { withCredentials: true });
  }

  public logout() {
    this.stopRefreshTokenTimer();
    var refreshToken = localStorage.getItem(this.REFRESH_TOKEN_KEY);
    if (refreshToken == null || refreshToken.length == 0) {
      this.userSubject.next(null);
      return new Observable((o) => o.complete());
    }

    return this.http.post<any>(this.API_LOGOUT_URL, { "refreshToken": refreshToken }, { withCredentials: true })
      .pipe(map((data) => {
        localStorage.removeItem(this.REFRESH_TOKEN_KEY);
        this.userSubject.next(null);
        return data;
      }));
  }

  private refreshTokenTimeout;

  private startRefreshTokenTimer() {
    const jwtToken = JSON.parse(atob(this.userValue.accessToken.split('.')[1]));
    const expires = new Date(jwtToken.exp * 1000);
    const timeout = expires.getTime() - Date.now() - (30 * 1000);
    this.refreshTokenTimeout = setTimeout(() => this.refreshToken().subscribe({
      next: (data) => {
        this.userSubject.next(data);
        this.startRefreshTokenTimer();
      }, 
      error: (error) => {
        this.userSubject.next(null);
        this.stopRefreshTokenTimer();
        this.router.navigate(["/login"]);
      }
    }), timeout);
  }

  private stopRefreshTokenTimer() {
    clearTimeout(this.refreshTokenTimeout);
  }
}
