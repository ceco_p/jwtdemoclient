import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { first } from 'rxjs/operators';
import { AuthService } from '../../services/auth.service';
 
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'] 
})
export class LoginComponent implements OnInit {

  public user: string;
  public password: string;

  private returnUrl: string;
  
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private authService: AuthService) { 
      if(authService.userValue != null) {
        router.navigate(["/"]);
      }
    }

  ngOnInit(): void {
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  public login() {
    this.authService.login(this.user, this.password)
      .pipe(first())
      .subscribe({
        next: () => {
          this.router.navigate([this.returnUrl]);
        },
        error: e => {
          console.log(e);
        }
      });
      
  }

}
